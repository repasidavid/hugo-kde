/*
 * SPDX-FileCopyrightText: 2022 Nguyen Hung Phu <phu.nguyen@kdemail.net>
 * SPDX-License-Identifier: LGPL-3.0-or-later
 */

(function() {
    var sttLabel = document.getElementById('status');

    var match = window.location.href.match(/\?s=([^&]+)/);
    if (!match) {
        sttLabel.innerText = sttLabel.dataset.invalid;
        return;
    }

    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
    function escapeRegExp(string) {
        return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
    }

    var query = decodeURIComponent(match[1].replace(/\+/g, ' '));
    // set values of both 2 'search-input's
    Array.prototype.forEach.call(document.getElementsByClassName('search-input'), (e) => e.value = query);
    var pattern = new RegExp(escapeRegExp(query), 'i');

    var KEYS = ['title', 'tags', 'categories', 'content', 'permalink', 'metadata'];
    // the 'excluded_keys' in hugo-i18n's config.py combined with keys that are already searched
    // TODO: get custom 'excludedKeys' of each project config.yaml, put in a data attribute and combine with this array. But is it always a suitable thing to do?
    var EXCLUDED_KEYS = ['aliases', 'appstream', 'authors', 'background', 'date', 'cdnJsFiles', 'flatpak_exp', 'forum', 'jsFiles', 'konqi', 'layout', 'parent', 'publishDate', 'sassFiles', 'screenshot', 'SPDX-License-Identifier', 'src_icon', 'type', 'url', 'userbase', 'categories', 'tags', 'title'];

    // search in frontmatter
    function searchInObj(item) {
        if (typeof item === 'string') {
            return (item.split(' ').length > 1 && pattern.test(item));
        } else if (item.constructor === Array) {
            return item.some(e => e? searchInObj(e) : false);
        } else if (item.constructor === Object) {
            for (var key of Object.keys(item)) {
                if (EXCLUDED_KEYS.includes(key) || !item[key]) {
                    continue;
                }
                if (searchInObj(item[key])) {
                    return true;
                }
            }
            return false;
        }
    }

    // Gregory Anders' "Building Site Search With Hugo" https://gpanders.com/blog/building-site-search-with-hugo/
    function search(item) {
        for (var key of KEYS) {
            if (!item[key]) {
                continue;
            }

            if (key === 'tags' || key === 'categories') {
                if (item[key].some(term => pattern.test(term))) {
                    return true;
                }
            } else if (key === 'metadata') {
                if (searchInObj(item[key])) {
                    return true;
                }
            } else if (pattern.test(item[key])) {
                return true;
            }
        }

        return false;
    }

    // eddiewebb's "Client side searching for Hugo.io with Fuse.js" https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae
    // jzeneto's conversion to vanilla JavaScript https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae#gistcomment-2987774
    function render(templateString, data) {
        var conditionalMatches, conditionalPattern, copy;
        conditionalPattern = /\$\{\s*isset ([a-zA-Z]*) \s*\}(.*)\$\{\s*end\s*}/g;
        // since loop below depends on re.lastIndex, we use a copy to capture any manipulations whilst inside the loop
        copy = templateString;
        while ((conditionalMatches = conditionalPattern.exec(templateString)) !== null) {
            if (data[conditionalMatches[1]]) {
                //valid key, remove conditionals, leave content.
                copy = copy.replace(conditionalMatches[0], conditionalMatches[2]);
            } else {
                //not valid, remove entire section
                copy = copy.replace(conditionalMatches[0], '');
            }
        }
        templateString = copy;
        //now any conditionals removed we can do simple substitution
        var key, find, re;
        for (key in data) {
            find = '\\$\\{\\s*' + key + '\\s*\\}';
            re = new RegExp(find, 'g');
            templateString = templateString.replace(re, data[key]);
        }
        return templateString;
    }

    /**
     * By Mark Amery: https://stackoverflow.com/a/35385518
     * @param {String} HTML representing a single element
     * @return {Element}
     */
    function htmlToElement(html) {
        var template = document.createElement('template');
        html = html.trim(); // Never return a text node of whitespace as the result
        template.innerHTML = html;
        return template.content.firstChild;
    }

    var lang = document.querySelector('html').getAttribute('lang');
    var langPrefix = lang == 'en'? sttLabel.dataset.langprefix : '/' + lang;

    var httpRequest = new XMLHttpRequest();
    httpRequest.onerror = function() {
        sttLabel.innerText = sttLabel.dataset.networkerror;
        console.log('Network error: ' + httpRequest.status);
    }
    httpRequest.onload = function() {
        if (httpRequest.status < 200 || httpRequest.status >= 400) {
            sttLabel.innerText = sttLabel.dataset.error;
            console.log('Target reached with error ' + httpRequest.status);
            return;
        }

        var results = JSON.parse(httpRequest.responseText).filter(search);
        if (results.length == 0) {
            sttLabel.innerText = sttLabel.dataset.noresult;
            return;
        }
        // sort by date and title
        results.sort((a,b) => {
            var aDate = a.metadata.date, bDate = b.metadata.date;
            if (aDate && bDate) {
                var timeDiff = Date.parse(bDate) - Date.parse(aDate)
                return (timeDiff != 0? timeDiff : a.title.localeCompare(b.title));
            } else if (aDate) {
                return -1;
            } else if (bDate) {
                return 1;
            } else {
                return a.title.localeCompare(b.title);
            }
        });

        sttLabel.innerText = '';
        // populate results
        for (var i = 0; i < results.length; i++) {
            // pull template from hugo template definition
            var templateDefinition = document.getElementById('search-result-template').innerHTML;
            var date = Date.parse(results[i].metadata.date);
            var permalink = window.location.hostname == 'planet.kde.org'? langPrefix + results[i].permalink : results[i].permalink;
            // replace values
            var output = render(templateDefinition, {
                key: i,
                title: results[i].title,
                link: permalink,
                date: date? new Date(date).toLocaleDateString('en-ZA') : null,
                tags: results[i].tags,
                categories: results[i].categories
            });
            document.getElementById('search-results').appendChild(htmlToElement(output));
        };
    };

    sttLabel.innerText = sttLabel.dataset.searching;
    httpRequest.open('GET', langPrefix + '/index.jsn');
    httpRequest.send();
})();
